#!/usr/bin/env node
'use strict';

// See https://git.coolaj86.com/coolaj86/acme-challenge-test.js
var tester = require('acme-challenge-test');
require('dotenv').config();

// Usage: node ./test.js example.com xxxxxxxxx
var zone = process.argv[2] || process.env.DUCKDNS_DOMAIN
var challenger = require('./index.js').create({
	token: process.argv[3] || process.env.DUCKDNS_TOKEN
});

// DuckDNS is limited and cannot pass the full range of tests
// so instead of the normal testZone(), we testRecord().
tester
	.testRecord('dns-01', zone, challenger)
	.then(function() {
		console.info('PASS', zone);
	})
	.catch(function(e) {
		console.error(e.message);
		console.error(e.stack);
	});
