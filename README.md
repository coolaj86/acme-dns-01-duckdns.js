# [acme-dns-01-duckdns](https://git.rootprojects.org/root/acme-dns-01-duckdns.js) | a [Root](https://rootprojects.org) project

DuckDNS + Let's Encrypt for Node.js

This handles ACME dns-01 challenges, compatible with ACME.js, Greenlock.js, and more.
Passes [acme-dns-01-test](https://git.rootprojects.org/root/acme-dns-01-test.js).

## Features

-   Compatible
    -   [x] Let's Encrypt v2.1 / ACME draft 18 (2019)
    -   [x] DuckDNS API
    -   [x] ACME.js, Greenlock.js, and others
-   Quality
    -   [x] node v6 compatible VanillaJS
    -   [x] &lt; 100 lines of code
    -   [x] **Zero External Dependencies**\*

<small>\* just `@root/request` which is also a Root project and has no dependencies at all</small>

# Install

```bash
npm install --save acme-dns-01-duckdns@3.x
```

Register and DuckDNS Domain and Save the API Token:

-   <https://www.duckdns.org/>

# Usage

Then you can use this with any compatible ACME library,
such as Greenlock.js or ACME.js.

### Greenlock CLI

```bash
npx greenlock defaults --challenge-dns-01 acme-dns-01-duckdns --challenge-dns-01-token xxxx
```

### Greenlock API

```js
greenlock.manager.defaults({
	challenges: {
		'dns-01': {
			module: 'acme-dns-01-duckdns',
	        baseUrl: 'https://www.duckdns.org/update', // default
			token: 'xxxxxxx'
		}
	}
});
```

See [Greenlock Express](https://git.rootprojects.org/root/greenlock-express.js)
and/or [Greenlock.js](https://git.rootprojects.org/root/greenlock.js)
documentation for more details.

### ACME.js

First you create an instance with your credentials:

```js
var dns01 = require('acme-dns-01-duckdns').create({
	baseUrl: 'https://www.duckdns.org/update', // default
	token: 'xxxx'
});
```

```js
acme.certificates.create({
	account,
	accountKey,
	csr,
	domains,
	challenges: { 'dns-01': dns01 }
});
```

See the [ACME.js](https://git.rootprojects.org/root/acme.js) for more details.

### Build your own

See [acme-dns-01-test](https://git.rootprojects.org/root/acme-dns-01-test.js)
for more implementation details.

# Tests

```bash
# node ./test.js domain-record api-token
node ./test.js example.duckdns.org xxxxxx
```

# Authors

-   Aneem Patrabansha
-   AJ ONeal

See AUTHORS for contact info.

<!-- {{ if .Legal }} -->

# Legal

[acme-dns-01-duckdns.js](https://git.coolaj86.com/coolaj86/acme-dns-01-duckdns.js) |
MPL-2.0 |
[Terms of Use](https://therootcompany.com/legal/#terms) |
[Privacy Policy](https://therootcompany.com/legal/#privacy)

Copyright 2019 The Root Group LLC

<!-- {{ end }} -->
